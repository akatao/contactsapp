ContactsApp Read Me
=================================

# DEVELOPER: Anderson Katao

# Description:
This sample ContactsApp was developed in iOS8 with xCode 7.0.1.
This sample Contacts app displays information about various contacts.

# 3rd-Party Used Libraries

- AFNetworking - AFNetworking is incredibly popular – it won our Reader’s Choice 2012 Best iOS Library Award. It’s also one of the most widely used, open-source projects with over 10,000 stars, 2,600 forks, and 160 contributors on Github.
In this project the AFNetworking was used to perform the GET operation of the contacts JSON in WebService. It was also used for serializing JSON.

- Magical Record - MagicalRecord, a third-party library for Core Data created by MagicalPanda. MagicalRecord provides convenience methods that wrap common boilerplate code for Core Data setup, query and update. It takes many design cues from the venerable Active Record design pattern. In this project the MagicalRecord was used to load, create and delete contacts from CoreData Stack.

- SDWebImage - SDWebImage is the best asynchronous image downloader. It is a solid project, with many contributors, that are maintained properly. In this project the SDWebImage was used to load the images of contacts asynchronously from the web service in the contact lists (tableView and colletionView) and the contact detail.

# How to generate this app

1. Clone the repository from terminal by using this command: git clone git@bitbucket.org:akatao/contactsapp.git
2. Open the file ContactsApp.xcworkspace from terminal: open ContactsApp.xcworkspace
3. Select the device you want to run the app
4. CMD + R or Run

# Structure & Code 

1. Contact List:
     1.1. Displays the contacts loaded from a JSON file in the WebSerivce;
     1.2. AFNetworking framework for asynchronous loading of the contacts shown in RestManager class, 
which is responsible for making the get request in the webservice;
     1.3. A custom cell was developed to present contacts in tableView (Fields: Profile picture, name and e-mail);
     1.4. It was imported another Contacts JSON file called otherContacts.json with contacts from A to Z, 
for future implementation of table with section separated by letter.

2. Favorite List:
     2.1. Only a representation of the same list of contacts but using the CollectionView;

3. Detail of Contact:
     3.1. Displays the details of the contact like profile picture, name, email, born and bio;

4. CRUD of contacts:
     4.1. The CoreDataManager class manages application data persistence code;
     4.2. The ContactDAO class is responsible and specialized in contacts CRUD. It is the way that the 
application interacts with the CoreData Stack;
     4.3. Contact and Contact+CoreDataProperties classes makes up the ContactVO;
     4.4. The NewEditContactViewController class is responsible for adding new contacts or edit existing contacts;

5. Animations:
5.1. When back from DetailContact in CollectionView
5.2. When save new user fade
5.3. When delete contact

# Notes:
     1. The application was developed following the standards of the design pattern MVC;
     2. Speaking of architecture the CoreData was implemented with separation of responsibilities (MVC and VO);
     3. Autolayout was used with constraints adjustments to run on all iPhone devices (4s, 5, 5s, 6, 6+, 6s and 6s+);
     4. It was used by third-party libraries Cocoapods;

# Next Steps:

1. Implement the search of contacts
2. Improvment of the collection contacts
3. Implement Sections in tableView and CollectionView
4. Implement the search for the collection too
5.


*Version 1.0*  
*October 15, 2015*

Updated for Xcode 7.0.1, iOS 8.0 SDK.