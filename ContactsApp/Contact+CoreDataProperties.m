//
//  Contact+CoreDataProperties.m
//  
//
//  Created by Anderson Katao on 10/3/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact+CoreDataProperties.h"

@implementation Contact (CoreDataProperties)

@dynamic photo;
@dynamic name;
@dynamic email;
@dynamic born;
@dynamic bio;

@end
