//
//  Animator.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/15/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Animator : NSObject <UIViewControllerAnimatedTransitioning>

@end
