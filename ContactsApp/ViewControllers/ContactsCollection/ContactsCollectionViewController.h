//
//  ContactsCollectionViewController.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/10/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsCollectionViewController : UICollectionViewController

@end
