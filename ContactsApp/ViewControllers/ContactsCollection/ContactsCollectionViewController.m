//
//  ContactsCollectionViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/10/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "ContactsCollectionViewController.h"
#import "ContactCollectionCell.h"
#import "Contact.h"
#import "ContactDAO.h"
#import "DetailContactViewController.h"
#import "NewEditContactViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContactsCollectionViewController ()

#pragma mark - Properties
/*
 * Selected Contact (Current) */
@property (nonatomic, strong) Contact *selectedContact;
/*
 * Selected Contact (Current) */
@property (nonatomic, strong) NSMutableArray *contacts;

@end

@implementation ContactsCollectionViewController

static NSString * const reuseIdentifier = @"cellIdentifier";

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:@"ContactCollectionCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:reuseIdentifier];
    
    // Load Contacts from CoreData
    [self fetchAllContacts];
    
    [self.collectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self handleUpdatedData:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self handleUpdatedData:nil];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.contacts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactCollectionCell *cell =
        (ContactCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Contact *contact = [self.contacts objectAtIndex:indexPath.row];
    
    if ([contact.photo isEqualToString:@"profile.png"]) {
        cell.imageViewProfile.image = [UIImage imageNamed:@"profile"];
    }
    else {
        // Load image from URL Asynchronous using SDWebImage
        [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:contact.photo]
                                 placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", contact.name]]];
    }
    
    cell.labelName.text = contact.name;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedContact = [self.contacts objectAtIndex:indexPath.row];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"collectionDetailSegue" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"collectionDetailSegue"]) {
        DetailContactViewController *detailContactViewController = segue.destinationViewController;
        detailContactViewController.contact = self.selectedContact;
    }
}

#pragma mark - IBAction

- (IBAction)performNewContact:(id)sender {
    NewEditContactViewController *newEditContactViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewEditContactViewController"];
    [self presentViewController:newEditContactViewController animated:YES completion:^{
        // Some completion
    }];
}


#pragma mark - Auxiliar

- (void)handleUpdatedData:(NSNotification *)notification {
    [self fetchAllContacts];
    [self reloadData];
}

- (void)reloadData {
    [self.collectionView reloadData];
}

- (void)fetchAllContacts {
    self.contacts = [NSMutableArray arrayWithArray:[ContactDAO fetchAllContacts]];
}

- (void)saveToPersistentStore {
    // Persist Contacts loaded in CoreData
    [[[CoreDataManager sharedInstance] managedObjectContext] MR_saveToPersistentStoreAndWait];
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
