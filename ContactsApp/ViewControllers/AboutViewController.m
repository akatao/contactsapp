//
//  AboutViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/15/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelAbout;
@property (weak, nonatomic) IBOutlet UILabel *labelHashtag;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle2;
@property (weak, nonatomic) IBOutlet UILabel *labelAfnetworking;
@property (weak, nonatomic) IBOutlet UILabel *labelMagicalRecord;
@property (weak, nonatomic) IBOutlet UILabel *labelSDWebImage;
@property (weak, nonatomic) IBOutlet UILabel *labelFooter;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLinkedin;
@property (weak, nonatomic) IBOutlet UILabel *labelAkatao;

@property (nonatomic, strong) CATransition *transition;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.alpha = 0.0f;
    self.labelTitle.alpha = 0.0f;
    self.labelAbout.alpha = 0.0f;
    self.labelHashtag.alpha = 0.0f;
    self.labelTitle2.alpha = 0.0f;
    self.labelAfnetworking.alpha = 0.0f;
    self.labelMagicalRecord.alpha = 0.0f;
    self.labelSDWebImage.alpha = 0.0f;
    self.labelFooter.alpha = 0.0f;
    self.imageViewLinkedin.alpha = 0.0f;
    self.labelAkatao.alpha = 0.0f;
    
    self.transition = [CATransition animation];
    self.transition.startProgress = 0;
    self.transition.endProgress = 1.0;
    self.transition.type = kCATransitionFade;
    self.transition.subtype = kCATransitionFromTop;
    self.transition.duration = 0.3;
    
    [UIView animateWithDuration:3
                     animations:^{
                         [self.view.layer addAnimation:self.transition forKey:@"transition"];
                         self.imageView.alpha = 1.0f;
                         self.labelTitle.alpha = 1.0f;
                         self.labelAbout.alpha = 1.0f;
                         self.labelHashtag.alpha = 1.0f;
                         self.labelTitle2.alpha = 1.0f;
                         self.labelAfnetworking.alpha = 1.0f;
                         self.labelMagicalRecord.alpha = 1.0f;
                         self.labelSDWebImage.alpha = 1.0f;
                         self.labelFooter.alpha = 1.0f;
                         self.imageViewLinkedin.alpha = 1.0f;
                         self.labelAkatao.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

@end
