//
//  SearchResultsViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/8/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "Contact.h"
#import "DetailContactViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *cellIdentifier = @"cellIdentifier";

@interface SearchResultsViewController ()

#pragma mark - Properties
/*
 * Selected Contact (Current) */
@property (nonatomic, strong) Contact *selectedContact;

#pragma mark - Animated Outlets



@end

@implementation SearchResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Contact *contact = [self.searchResults objectAtIndex:indexPath.row];

    cell.imageView.image = [UIImage imageNamed:contact.photo];
    cell.textLabel.text = contact.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedContact = [self.searchResults objectAtIndex:indexPath.row];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"searchDetailSegue" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"searchDetailSegue"]) {
        DetailContactViewController *detailContactViewController = segue.destinationViewController;
        detailContactViewController.contact = self.selectedContact;
    }
}

@end
