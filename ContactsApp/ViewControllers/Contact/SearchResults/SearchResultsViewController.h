//
//  SearchResultsViewController.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/8/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *searchResults; // Filtered search results

@end
