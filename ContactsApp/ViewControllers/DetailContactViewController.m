//
//  DetailContactViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/3/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "DetailContactViewController.h"
#import "NewEditContactViewController.h"
#import "AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailContactViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelBorn;
@property (weak, nonatomic) IBOutlet UITextView *labelBio;

@end

@implementation DetailContactViewController


#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadFields];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadFields];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        // back button was pressed
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:NO];
    }
}

- (void)loadFields {
    
    if (self.contact != nil) {
        if ([self.contact.photo isEqualToString:@"profile.png"]) {
            self.imageViewProfile.image = [UIImage imageNamed:@"profile"];
        }
        else {
            // Load image from URL Asynchronous using SDWebImage
            [self.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:self.contact.photo]
                                     placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", self.contact.name]]];
        }
        
        self.labelName.text = self.contact.name;
        self.labelEmail.text = self.contact.email;
        self.labelBorn.text = self.contact.born;
        self.labelBio.text = self.contact.bio;
    }
    else {
        NSLog(@"The contact is Nil");
    }
}

#pragma mark - IBActions

- (IBAction)prepareForEdit:(UIBarButtonItem *)sender {
    NewEditContactViewController *newEditContactViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewEditContactViewController"];
    newEditContactViewController.contact = self.contact;
    [self presentViewController:newEditContactViewController animated:YES completion:^{
        // Some completion
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"editSegue"]) {
        NewEditContactViewController *editContactViewController = segue.destinationViewController;
        editContactViewController.contact = self.contact;
    }
}

@end
