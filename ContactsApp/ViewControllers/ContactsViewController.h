//
//  ContactsViewController.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/3/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * A classe ContactsViewController é responsável por apresentar a lista de contatos
 * que foi carregada de um arquivo JSON do Webservice no CoreData. Esta classe herda 
 * de UITableViewController e utiliza a célula customizada ContactCell.
 * @author Anderson Katao
 */
@interface ContactsViewController : UITableViewController

@end
