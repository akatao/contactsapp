//
//  ContactsViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/3/15.
//  Copyright © 2015 AK. All rights reserved.
//

@import Foundation;

#import "ContactsViewController.h"
#import "DetailContactViewController.h"
#import "Contact+CoreDataProperties.h"
#import "AFNetworking.h"
#import "ContactCell.h"
#import "ContactDAO.h"
#import "SearchResultsViewController.h"
#import "NewEditContactViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *cellIdentifier = @"cellIdentifier";

@interface ContactsViewController () <UISearchResultsUpdating>

#pragma mark - Properties
/*
 * Selected Contact (Current) */
@property (nonatomic, strong) Contact *selectedContact;
/*
 * Contacts Array */
@property (nonatomic, strong) NSMutableArray *contacts;
/*
 * Filtered search results */
@property (nonatomic, strong) NSMutableArray *searchResults;
/*
 * Section Index titles */
@property (nonatomic, strong) NSMutableArray *indexTitles;
/*
 * Contact profile image */
@property (nonatomic, strong) UIImage *imageProfile;
/*
 * SearchController for perform search */
@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation ContactsViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    // Fetch All Contacts
    [self fetchAllContacts];
    
    // Register the Custom Contact table view cell
    [self.tableView registerNib:[UINib nibWithNibName:@"ContactCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
    
    
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    // Load Search Controller
//    [self prepareForSearch];
    
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self handleUpdatedData:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self handleUpdatedData:nil];
}

#pragma mark - TableView DataSource

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.contacts count];
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//    return self.indexTitles;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
//    return [self.indexTitles indexOfObject:title];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    Contact *contact = [self.contacts objectAtIndex:indexPath.row];
    
    if ([contact.photo isEqualToString:@"profile.png"]) {
        cell.imageViewProfile.image = [UIImage imageNamed:@"profile"];
    }
    else { // Load image from URL Asynchronous using SDWebImage
        [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:contact.photo]
                                 placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", contact.name]]];
    }
    
    cell.imageViewProfile.clipsToBounds = YES;
    cell.imageViewProfile.layer.cornerRadius = 27;
    cell.labelName.text = contact.name;
    cell.labelEmail.text = contact.email;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedContact = [self.contacts objectAtIndex:indexPath.row];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"detailSegue" sender:self];
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Contact *contact = [self.contacts objectAtIndex:indexPath.row];
        
        //1. Remove from Array
        [self.contacts removeObjectAtIndex:indexPath.row];
        
        //2. Remove from CoreData
        [ContactDAO removeContact:contact];
        
        //3. Remove from TableView
        [tableView deleteRowsAtIndexPaths:@[indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
    else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
    
    [self saveToPersistentStore];
    [self reloadData];
}

#pragma mark - Search Contacts

- (void)prepareForSearch {
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"SearchResultsNavController"];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
}

#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchString = self.searchController.searchBar.text;
    [self updateFilteredContentForContactName:searchString];
    
    if (self.searchController.searchResultsController) {
        
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        
        // Present SearchResultsTableViewController as the topViewController
        SearchResultsViewController *vc = (SearchResultsViewController *)navController.topViewController;
        
        // Update searchResults
        vc.searchResults = self.searchResults;
        
        // And reload the tableView with the new data
        [vc.tableView reloadData];
    }
}

- (void)updateFilteredContentForContactName:(NSString *)contactName {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", contactName];
    self.searchResults = [NSMutableArray arrayWithArray:[self.contacts filteredArrayUsingPredicate:resultPredicate]];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - IBaction

- (IBAction)performNewContact:(id)sender {
    NewEditContactViewController *newEditContactViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewEditContactViewController"];
    [self presentViewController:newEditContactViewController animated:YES completion:^{
        // Some completion
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        DetailContactViewController *detailContactViewController = segue.destinationViewController;
        detailContactViewController.contact = self.selectedContact;
    }
}

#pragma mark - Helper Methods

- (void)handleUpdatedData:(NSNotification *)notification {
    [self fetchAllContacts];
    [self reloadData];
}

- (void)reloadData {
    [self.tableView reloadData];
}

- (void)fetchAllContacts {
    _contacts = [NSMutableArray arrayWithArray:[ContactDAO fetchAllContacts]];
}

- (void)saveToPersistentStore {
    // Persist Contacts loaded in CoreData
    [[[CoreDataManager sharedInstance] managedObjectContext] MR_saveToPersistentStoreAndWait];
}

@end
