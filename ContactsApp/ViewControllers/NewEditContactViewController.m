//
//  NewEditContactViewController.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/3/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "NewEditContactViewController.h"
#import "DetailContactViewController.h"
#import "CoreDataManager.h"
#import "ContactDAO.h"

@interface NewEditContactViewController () <UITextFieldDelegate,
                                             UITextViewDelegate,
                                UIImagePickerControllerDelegate,
                                 UINavigationControllerDelegate,
                                            UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonSave;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;

@property (weak, nonatomic) IBOutlet UITextField *textFieldName;

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;

@property (weak, nonatomic) IBOutlet UITextField *textFieldBorn;

@property (weak, nonatomic) IBOutlet UITextView *textViewBio;

@property (weak, nonatomic) IBOutlet UILabel *labelBio;

@property (nonatomic, strong) DetailContactViewController *detailContactViewController;

@property (nonatomic, strong) CATransition *transition;

@end

@implementation NewEditContactViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.barButtonSave setEnabled:NO];
    
    self.detailContactViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailContactViewController"];
    
    self.textViewBio.delegate = self;
    
    self.detailContactViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"DetailContactViewController"];
    
    if (self.contact != nil) {
        // Edit Contact
        self.title = @"";
        
        self.imageViewProfile.image = [UIImage imageNamed:self.contact.photo];
        self.textFieldName.text = self.contact.name;
        self.textFieldEmail.text = self.contact.email;
        self.textFieldBorn.text = self.contact.born;
        self.textViewBio.text = self.contact.bio;
    }
    else {
        self.labelBio.hidden = NO;
    }
    
    self.transition = [CATransition animation];
    self.transition.startProgress = 0;
    self.transition.endProgress = 1.0;
    self.transition.type = kCATransitionFade;
    self.transition.subtype = kCATransitionFromTop;
    self.transition.duration = 0.3;
}

#pragma mark - Auxiliar

- (void)setupNewContact {
    
#warning @TODO: Get the selected profile image, save in contact
    self.contact.photo = @"profile.png";
    // Check only the name field
    if (![self.textFieldName.text isEqualToString:@""]) {
        self.contact.name = self.textFieldName.text;
    }
    else {
        
    }
    
    if (![self.textFieldEmail.text isEqualToString:@""]) {
        self.contact.email = self.textFieldEmail.text;
    }
    else {
        self.contact.email = @"";
    }
    
    if (![self.textFieldBorn.text isEqualToString:@""]) {
        self.contact.born = self.textFieldBorn.text;
    }
    else {
        self.contact.born = @"";
    }
    if (![self.textViewBio.text isEqualToString:@""]) {
        self.contact.bio = self.textViewBio.text;
    }
    else {
        self.contact.bio = @"";
    }
}


#pragma mark - IBActions

- (IBAction)saveAction:(id)sender {
    
    // Editing Contact
    if (self.contact) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            [self setupNewContact];
            [self saveToPersistentStore];
        }
        completion:^(BOOL contextDidSave, NSError *error) {
            NSLog(@"Error: %@", error);
            [self checkRequiredNameField];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }// Adding New Contact
    else {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            self.contact = [ContactDAO contactDao];
            [self setupNewContact];
            [self saveToPersistentStore];
            NSLog(@"Save with Success");
        }
        completion:^(BOOL contextDidSave, NSError *error) {
            NSLog(@"Error: %@", error);
            [self checkRequiredNameField];
        }];
    }
}

- (void)checkRequiredNameField {
    if (self.contact.name != nil && ![self.contact.name isEqualToString:@""]) {
        self.detailContactViewController.contact = self.contact;
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.view.alpha = 1.0f;
                             [self.view.layer addAnimation:self.transition forKey:@"transition"];
                         }
                         completion:^(BOOL finished){
                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Required field"
                              message:@"Fill the name field"
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil,nil];
        
        [alert show];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)addPhotoAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
    
    [alert show];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    NSLog(@"textFieldDidBeginEditing");
    [self.barButtonSave setEnabled:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"textFieldDidEndEditing");
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        self.labelBio.hidden = YES;
    }
    NSLog(@"Begin editing");
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        self.labelBio.hidden = NO;
    }
    NSLog(@"DidEndEditing");
}

#pragma mark - Auxiliar Methods

- (void)takePhoto {
    // Check if the device has camera
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)selectPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)saveToPersistentStore {
    // Persist Contacts loaded in CoreData
    [[[CoreDataManager sharedInstance] managedObjectContext] MR_saveToPersistentStoreAndWait];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageViewProfile.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self takePhoto];
    }
    else if (buttonIndex == 2) {
        [self selectPhoto];
    }
}

@end
