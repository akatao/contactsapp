//
//  DetailContactViewController.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/3/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact+CoreDataProperties.h"

//@protocol DetailContactViewControllerDelegate <NSObject>
//
//- (void)didFinishSaveContact;
//
//@end

@interface DetailContactViewController : UIViewController

@property (nonatomic, strong) Contact *contact;

//@property (nonatomic, weak) id<DetailContactViewControllerDelegate> delegate;

@end
