//
//  ContactDAO.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/14/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "ContactDAO.h"

@implementation ContactDAO

+ (Contact *)contactDao {
    return [Contact MR_createEntityInContext:[[CoreDataManager sharedInstance] managedObjectContext]];
}

+ (BOOL)truncateAll {
    if ([Contact MR_truncateAll]) {
        NSLog(@"Truncate All Successfully.");
        return YES;
    }
    NSLog(@"Truncate All Successfully.");
    return NO;
}

+ (NSArray *)fetchAllContacts {
    return [Contact MR_findAllSortedBy:@"name"
                             ascending:YES
                             inContext:[[CoreDataManager sharedInstance] managedObjectContext]];
}

+ (BOOL)importFromDict:(NSDictionary *)contactDict {
    Contact *contact = [Contact MR_importFromObject:contactDict
                                          inContext:[[CoreDataManager sharedInstance] managedObjectContext]];
    if (contact != nil) {
        NSLog(@"Contact imported successfully.");
        return YES;
    }
    NSLog(@"Contact imported failed.");
    return NO;
}

+ (BOOL)removeContact:(Contact *)contact {
    if ([contact MR_deleteEntityInContext:[[CoreDataManager sharedInstance] managedObjectContext]]) {
        NSLog(@"Delete contact successfully.");
        return YES;
    }
    NSLog(@"Delete contact failed.");
    return NO;
}

@end
