//
//  ContactDAO.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/14/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "CoreDataManager.h"
#import <MagicalRecord/MagicalRecord.h>

/*
 * This class is responsible and specialized in contacts CRUD. It is the way that the
 * application interacts with the CoreData Stack.
 * @author Anderson Katao
 */
@interface ContactDAO : NSObject

/*
 * Instantiate a new contact into Contacts stack. 
 * @return New instance of contact*/
+ (Contact *)contactDao;

/*
 * Truncate the Contacts CoreData Stack. 
 * @return BOOL - Return true or false when truncate Contacts */
+ (BOOL)truncateAll;

/*
 * Fetch all Contacts into CoreData. 
 * @return: NSArray - Return an array with all contacts from CoreData */
+ (NSArray *)fetchAllContacts;

/*
 * Import a NSDictionary into Contacts stack.
 * @param contactDict - Contact NSDictionary.
 * @return: BOOL*/
+ (BOOL)importFromDict:(NSDictionary *)contactDict;

/*
 * Remove a contact from Contacts stack.
 * @param contact - Contact to be removed.
 * @return: BOOL - return true or false when removing the selected contact */
+ (BOOL)removeContact:(Contact *)contact;

@end
