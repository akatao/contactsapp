//
//  CoreDataManager.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/13/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/*
 * The CoreDataManager class manages application data persistence code.
 * @author Anderson Katao
 */
@interface CoreDataManager : NSObject

@property (nonatomic, readonly, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, readonly, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, readonly, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataManager *)sharedInstance;
- (BOOL)save;
- (NSManagedObjectContext*)managedObjectContext;

@end
