//
//  RestManager.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/14/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestManager : NSObject

/*
 * This method get all contacts from WEB via https request of a JSON endpoint
 * and populate the Contacts CoreData Stack. */
+ (void)getContactsFromWebServiceAndImportThemIntoCoreData;

@end
