//
//  RestManager.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/14/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "RestManager.h"
#import "AFNetworking.h"
#import "CoreDataManager.h"
#import "ContactDAO.h"

static NSString *const jsonURL = @"https://s3-sa-east-1.amazonaws.com/rgasp-mobile-test/v1/content.json";

@implementation RestManager

+ (void)getContactsFromWebServiceAndImportThemIntoCoreData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:jsonURL
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             // Load contacts from URL JSON file
             NSArray *contacts = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
             
             // Truncate CoreData Contacts
             [ContactDAO truncateAll];
             
             // Import contacts to CoreData
             for (NSDictionary *dataDic in contacts) {
                 [ContactDAO importFromDict:dataDic];
             }
             
             // Load contacts from local JSON file
             NSString *filePath = [[NSBundle mainBundle] pathForResource:@"otherContacts" ofType:@"json"];
             NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
             NSArray *contacts2 = (NSArray *)[NSJSONSerialization
                                              JSONObjectWithData:jsonData
                                              options:0 error:NULL];
             // Import other fake contacts to CoreData
             for (NSDictionary *contact in contacts2) {
                 [ContactDAO importFromDict:contact];
             }
             
             // Persist Contacts loaded in CoreData
             [[[CoreDataManager sharedInstance] managedObjectContext] MR_saveToPersistentStoreAndWait];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated" object:nil];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

@end
