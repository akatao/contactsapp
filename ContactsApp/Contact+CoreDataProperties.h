//
//  Contact+CoreDataProperties.h
//  
//
//  Created by Anderson Katao on 10/3/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact.h"

NS_ASSUME_NONNULL_BEGIN

@interface Contact (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *photo;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *born;
@property (nullable, nonatomic, retain) NSString *bio;

@end

NS_ASSUME_NONNULL_END
