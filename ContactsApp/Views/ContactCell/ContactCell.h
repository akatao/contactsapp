//
//  ContactCell.h
//  ContactsApp
//
//  Created by Anderson Katao on 10/8/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * The class is the custom ContactCell.
 * This cell displays the contact information like profile picture, name and e-mail
 * in the tableViewCell
 * @author Anderson Katao
 */
@interface ContactCell : UITableViewCell

#pragma mark - Outlets

/*
 * Profile Picture ImageView */
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;

/*
 * Name label */
@property (weak, nonatomic) IBOutlet UILabel *labelName;

/*
 * E-mail label */
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;

@end
