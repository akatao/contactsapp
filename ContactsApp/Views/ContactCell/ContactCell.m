//
//  ContactCell.m
//  ContactsApp
//
//  Created by Anderson Katao on 10/8/15.
//  Copyright © 2015 AK. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
